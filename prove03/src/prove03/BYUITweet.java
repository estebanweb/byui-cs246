package prove03;

import java.util.Comparator;

public class BYUITweet implements Comparable<BYUITweet>{

    private User user;
    private String text;

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    @Override
    public int compareTo(BYUITweet o) {
        return  (this.getUser().getFollowers() >= o.getUser().getFollowers()  )?1:-1;
    }
}
