package prove03;

import java.util.*;
import java.util.stream.Collectors;

public class Main {

    public static void main(String[] args) {

        TweetLoader tl = new TweetLoader();

        Map<String,BYUITweet> tweets =  tl.retrieveTweetsWithHashTag("#byui");

       System.out.println("Tweets:");
        for (Map.Entry<String, BYUITweet> entry : tweets.entrySet()) {
            System.out.println(String.format("%s (%d Followers) - %s \n",
                    entry.getKey(), entry.getValue().getUser().getFollowers(),entry.getValue().getText()));
        }


        Map<String,BYUITweet> sortedMap = sortByValue(tweets);
        System.out.println("\n\nTweets sorted by followers:");
        for (Map.Entry<String, BYUITweet> entry : sortedMap.entrySet()) {
            System.out.println(String.format("%s (%d Followers) - %s \n",
                    entry.getKey(), entry.getValue().getUser().getFollowers(),entry.getValue().getText()));
        }


    }


    private static Map<String, BYUITweet> sortByValue(Map<String, BYUITweet> unsortMap) {

        // 1. Convert Map to List of Map
        List<Map.Entry<String, BYUITweet>> list =
                new LinkedList<Map.Entry<String, BYUITweet>>(unsortMap.entrySet());

        // 2. Sort list with Collections.sort(), provide a custom Comparator
        //    Try switch the o1 o2 position for a different order
        Collections.sort(list, new Comparator<Map.Entry<String, BYUITweet>>() {
            public int compare(Map.Entry<String, BYUITweet> o1,
                               Map.Entry<String, BYUITweet> o2) {
                return (o1.getValue()).compareTo(o2.getValue());
            }
        });

        // 3. Loop the sorted list and put it into a new insertion order Map LinkedHashMap
        Map<String, BYUITweet> sortedMap = new LinkedHashMap<String, BYUITweet>();
        for (Map.Entry<String, BYUITweet> entry : list) {
            sortedMap.put(entry.getKey(), entry.getValue());
        }




        return sortedMap;
    }
}
