import java.util.Scanner;
import java.text.DecimalFormat;

public class CircleCalculator {

    private float radius;
    private String format = "##.00";
    private DecimalFormat formater;


    public  CircleCalculator(){
        this.formater = new DecimalFormat(this.format);
    }

    public void setRadius(float radius) {
        this.radius = radius;
    }

    public float getRadius() {
        return this.radius;
    }

    protected String applyDecimalFormat ( double value ){
        return this.formater.format(value);
    }

    public float askRadius() {

        Scanner scanner = new Scanner( System.in );
        System.out.print( "Please type the radius: " );
        String input = scanner.nextLine();

        return Float.parseFloat(input);
    }

    public void displayCircumference(float radius){

        double circumference= Math.PI * 2 * radius;
        System.out.println( "The circumference is: " + this.applyDecimalFormat(circumference)) ;
    }

    public void displayArea(float radius){

        double area = Math.PI * (radius * radius);
        System.out.println("The area is: " + this.applyDecimalFormat(area));
    }

    public static void main(String[] args) {

        CircleCalculator cc = new CircleCalculator();
        cc.setRadius(cc.askRadius());
        cc.displayArea(cc.getRadius());
        cc.displayCircumference(cc.getRadius());
    }

}
