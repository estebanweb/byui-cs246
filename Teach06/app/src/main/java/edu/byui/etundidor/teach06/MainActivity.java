package edu.byui.etundidor.teach06;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.LinkedList;

public class MainActivity extends AppCompatActivity {

    private String filename = "numbers.txt";
    private ArrayAdapter<String>  itemsAdapter;
    private Context context;
    private ProgressBar progressBar;
    private ListView lv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        context = this.getApplicationContext();
        progressBar = (ProgressBar)  findViewById(R.id.progressBar);
    }

    protected void createAction(View view){

        /*
        // Part One:

        try {
            FileOutputStream outputStream = new FileOutputStream(new File(getFilesDir(), filename));

            for(int i=1 ; i <= 10; i ++){
                String line = Integer.toString(i) + "\n";
                outputStream.write(line.getBytes());
                Thread.sleep(250);
            }

            outputStream.close();

            Toast toast = Toast.makeText(context, "File Created", Toast.LENGTH_SHORT);
            toast.show();

        }catch (Exception e){
            Toast toast = Toast.makeText(context, "unable to create the file", Toast.LENGTH_SHORT);
            toast.show();
        }
        */

        // Part Two:
        new CreateTask(filename,context,progressBar).execute();

    }

    protected void loadAction(View view){

        /*
        // Part One:
        FileInputStream is;
        BufferedReader reader;
        LinkedList<String> lines = new LinkedList<String>();

        final File file = new File(getFilesDir(), filename);

        try {
            is = new FileInputStream(file);
            reader = new BufferedReader(new InputStreamReader(is));
            String line = reader.readLine();
            while (line != null) {
                Log.d("BYUI", line);
                lines.add(line);
                Thread.sleep(250);
                line = reader.readLine();
            }

            ListView lv = (ListView) findViewById(R.id.items);
            itemsAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, lines);
            lv.setAdapter(itemsAdapter);


        }catch (Exception e){
            Toast toast = Toast.makeText(context, "Unable to read the file", Toast.LENGTH_SHORT);
            toast.show();
        }
        */

        // Part Two:
        lv = (ListView) findViewById(R.id.items);

        itemsAdapter = new ArrayAdapter<String>(context, android.R.layout.simple_list_item_1,new ArrayList<String>());
        lv.setAdapter(itemsAdapter);
        new LoadTask(filename,context,progressBar,itemsAdapter).execute();

    }

    protected void clearAction(View view){
        // Part One:
        itemsAdapter.clear();

    }

    private String readFromFile(Context context) {

        String ret = "";

        try {
            InputStream inputStream = context.openFileInput(filename);

            if ( inputStream != null ) {
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                String receiveString = "";
                StringBuilder stringBuilder = new StringBuilder();

                while ( (receiveString = bufferedReader.readLine()) != null ) {
                    stringBuilder.append(receiveString);
                }

                inputStream.close();
                ret = stringBuilder.toString();
            }
        }
        catch (FileNotFoundException e) {
            Log.e("login activity", "File not found: " + e.toString());
        } catch (IOException e) {
            Log.e("login activity", "Can not read file: " + e.toString());
        }

        return ret;
    }
}
