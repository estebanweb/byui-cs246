package edu.byui.etundidor.teach06;

import android.content.Context;
import android.os.AsyncTask;
import android.widget.ProgressBar;
import android.widget.Toast;

import java.io.File;
import java.io.FileOutputStream;

/**
 * Created by etundidor on 2/12/18.
 */

public class CreateTask extends AsyncTask<Void, Integer, Void> {


    String filename;
    Context context;
    ProgressBar progressBar;

    public CreateTask(String pFilename, Context cont, ProgressBar bar)
    {
        progressBar = bar;
        context = cont;
        filename = pFilename;
    }

    @Override
    protected Void doInBackground(Void... params) {

        try {
            FileOutputStream outputStream = new FileOutputStream(new File(context.getFilesDir(), filename));

            for(int i=1 ; i <= 10; i ++){
                String line = Integer.toString(i) + "\n";
                outputStream.write(line.getBytes());
                Thread.sleep(250);
                publishProgress(i*10);
            }

            outputStream.close();

        }catch (Exception e){
            Toast toast = Toast.makeText(context, "unable to create the file", Toast.LENGTH_SHORT);
            toast.show();
        }

        return null;
    }

    @Override
    protected void onPreExecute() {

        progressBar.setProgress(0);

        Toast toast = Toast.makeText(context, "Creating...", Toast.LENGTH_SHORT);
        toast.show();
    }


    @Override
    protected void onProgressUpdate(Integer... data) {
        progressBar.setProgress(data[0]);
    }

    @Override
    protected void onPostExecute(Void result) {


        progressBar.setProgress(100);

        Toast toast = Toast.makeText(context, "File Created Successfully.", Toast.LENGTH_SHORT);
        toast.show();
    }


}
