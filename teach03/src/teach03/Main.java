package teach03;

public class Main {

    public static void main(String[] args) {

        Player player = new Player();
        player.setGold(100);
        player.setHealth(1000);
        player.setMana(20);
        player.setName("Esteban");

        player.addEquipment("bag",1);
        player.addEquipment("food",10);
        player.addEquipment("shoes",2);
        player.addEquipment("jeans",1);
        player.addEquipment("t-shirts",5);


        Game game = new Game(player);
        game.saveGame("saved.json");

        Game game2 = Game.loadGame("saved.json");
        game2.getPlayer().displayAll();
    }
}