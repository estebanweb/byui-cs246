package teach03;

import com.google.gson.Gson;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Paths;

public class Game {

    private Player player;
    private Gson jsonParser;
    private String savedGame = "";

    Game(){
        this.setPlayer(new Player());
        this.setJsonParser(new Gson());
    }

    Game (Player player){
        this.setPlayer(player);
        this.setJsonParser(new Gson());
    }

    public boolean saveGame(String filename){

        this.setSavedGame(this.getJsonParser().toJson(this.getPlayer()));

        try {
            PrintWriter writer = new PrintWriter(filename);
            writer.println(this.getSavedGame());
            writer.close();

        } catch (FileNotFoundException e) {
            return false;
        }

        return  true;

    }

    public static Game loadGame(String filename){
        Player player;

        try {
            String content = new String(Files.readAllBytes(Paths.get(filename)));
            player = (new Gson()).fromJson(content,Player.class);

        }catch (Exception e){
            player = new Player();
        }

        return new Game(player);
    }

    public Gson getJsonParser() {
        return jsonParser;
    }

    public void setJsonParser(Gson jsonParser) {
        this.jsonParser = jsonParser;
    }

    public String getSavedGame() {
        return savedGame;
    }

    public void setSavedGame(String savedGame) {
        this.savedGame = savedGame;
    }

    public Player getPlayer() {
        return player;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }

}
