package teach03;

import java.util.HashMap;
import java.util.Map;

public class Player {
    protected String name;
    protected Integer health;
    protected Integer mana;
    protected Integer gold;
    protected Map<String,Integer> equipment;

    public Player() {
        Map<String,Integer> equipment = new HashMap<String, Integer>();
        this.setEquipment(equipment);
    }



    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getHealth() {
        return health;
    }

    public void setHealth(Integer health) {
        this.health = health;
    }

    public Integer getMana() {
        return mana;
    }

    public void setMana(Integer mana) {
        this.mana = mana;
    }

    public Integer getGold() {
        return gold;
    }

    public void setGold(Integer gold) {
        this.gold = gold;
    }

    public Map<String, Integer> getEquipment() {
        return equipment;
    }

    public void setEquipment(Map<String, Integer> equipment) {
        this.equipment = equipment;
    }


    public void addEquipment(String name, Integer amount){
        this.getEquipment().put(name,amount);
    }


    public void displayAll(){
        System.out.println("Name: " + this.getName());
        System.out.println("Healt: " + this.getHealth());
        System.out.println("Mana: " + this.getMana());
        System.out.println("Gold: " + this.getGold());

        System.out.println("Equipment: ");

        for (Map.Entry<String, Integer> entry : this.getEquipment().entrySet()) {
            System.out.println("\t" + entry.getValue() + " " + entry.getKey());
        }
    }
}
