package prove02;

import java.awt.*;

public class Zombie extends Creature implements Movable,Aggressor {


    public Zombie() {
        _health = 1;
    }

    @Override
    public void attack(Creature target) {
        // Zombies should attack any creature they land on, as long as it isn't a Plant.
        // Give the target 10 points of damage
        if (!(target instanceof Plant) && (target instanceof Creature)) {
            target.takeDamage(10);
        }
    }

    @Override
    Shape getShape() {
        return Shape.Square;
    }

    @Override
    Color getColor() {
        return new Color(0, 0, 255);
    }

    @Override
    Boolean isAlive() {
        return _health > 0;
    }

    @Override
    public void move() {
        _location.x++;
    }
}
