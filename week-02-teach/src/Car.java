public class Car extends ExpensePerDay {

    public Car(Destination destination){
        super(destination);
    }

    public Car(Destination destination, int days){
        super(destination,days);
    }

    protected float getMexicoCost(){
        return 50;
    }

    protected float getEuropeCost(){
        return 60;
    }

    protected float getJapanCost(){
        return 70;
    }
}
