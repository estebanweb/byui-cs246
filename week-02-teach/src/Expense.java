public class Expense implements Expenses {

    protected float cost = 0;
    protected Destination destination;

    public Expense (){}

    public Expense ( Destination dest ){
        this.destination = dest;
    }

    @Override
    public float getCost() {

        if (destination != null) {
            switch (this.destination){
                case Mexico:
                    cost = getMexicoCost();
                    break;
                case Europe:
                    cost = getEuropeCost();
                    break;
                case Japan:
                    cost = getJapanCost();
                    break;
            }
        }

        return cost;
    }

    protected float getMexicoCost(){
        return 100;
    }

    protected float getEuropeCost(){
        return 200;
    }

    protected float getJapanCost(){
        return 300;
    }
}
