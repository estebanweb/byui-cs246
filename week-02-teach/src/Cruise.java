public class Cruise extends Expense {

    private Destination destination;

    public Cruise ( Destination dest ){
        this.destination = dest;
    }

    protected float getMexicoCost(){
        return 1000;
    }

    protected float getEuropeCost(){
        return 2000;
    }

    protected float getJapanCost(){
        return 3000;
    }
}
