package tundidor.src;

/**
 * The WeakPasswordException must be thrown when the password doesn't fulfill the basic parameters
 *
 * @author  Esteban Tundidor
 * @version 1.0
 * @since   2018-01-13
 *
 */

public class WeakPasswordException extends Exception {
}
