package tundidor.src;

/**
 * The User is a representation of a NSA user
 *
 * @author  Esteban Tundidor
 * @version 1.0
 * @since   2018-01-13
 *
 */
public class User {
    private String password;
    private String salt;
    private String hashPassword;


    public User (String pass){
        this.setPassword(pass);
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getSalt() {
        return salt;
    }

    public void setSalt(String salt) {
        this.salt = salt;
    }

    public void setHashedPassword(String pass){
        this.hashPassword = pass;
    }

    public String getHashedPassword() {
        return this.hashPassword;
    }
}
