package edu.byui.etundidor.prove05;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    public static final String BOOK = "edu.byui.etundidor.prove05.BOOK";
    public static final String CHAPTER = "edu.byui.etundidor.prove05.CHAPTER";
    public static final String VERSE = "edu.byui.etundidor.prove05.VERSE";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void loadData(View view) {
        SharedPreferences sharedPref = this.getSharedPreferences("Scripture",Context.MODE_PRIVATE);
        String book = sharedPref.getString(BOOK, "Esteban");
        String chapter = sharedPref.getString(CHAPTER, "1");
        String verse = sharedPref.getString(VERSE, "1");
        EditText bookText = (EditText) findViewById(R.id.bookText);
        EditText chapterText = (EditText) findViewById(R.id.chapterText);
        EditText verseText = (EditText) findViewById(R.id.verseText);

        bookText.setText(book);
        chapterText.setText(chapter);
        verseText.setText(verse);

    }

    public void sendData(View view) {
        Intent intent = new Intent(this, DisplayMessageActivity.class);
        EditText bookText = (EditText) findViewById(R.id.bookText);
        EditText chapterText = (EditText) findViewById(R.id.chapterText);
        EditText verseText = (EditText) findViewById(R.id.verseText);


        Log.i("BYUI","About to create intent with "
                + bookText.getText().toString()
                + " "
                + chapterText.getText().toString()
                + ":"
                + verseText.getText().toString() );

        intent.putExtra(BOOK, bookText.getText().toString());
        intent.putExtra(CHAPTER, chapterText.getText().toString());
        intent.putExtra(VERSE, verseText.getText().toString());
        startActivity(intent);
    }

}
